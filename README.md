# General Discussion Board for Fandoms
You are here in the `gitlab.com:ThePinsTeam-CommunityHubNetwork/fandoms/general` repository where
general fandom discussions falls here.

## What You Can Do Here?
- **Request to add new fandom Issue Tracker.** Details are in the `docs/adding-your-fandoms.md` file and
on <https://thepinsteam-communityubnetwork.gitlab.io/fandoms/general/adding-your-fandoms>.

## How To Contribute?
Most of the contributions you can do is on [our GitLab Issue Tracker]. For documentation and improving the site,
there's a Merge Requests for that.

## Maintainers
| Maintainer Name | GitLab Username | Status | Maintainer since |
| ----- | ----- | ----- | ----- |
| Andrei Jiroh | @AndreiJirohHaliliDev2006 | **Active/The Pins Team Member** | Early days of this Issue Tracker |

### Apply for maintainership
[Check out our English handbook](https://en.handbooksbythepins.gq/community-hub/fandoms/issue-trakcers/maintainerships#application)
for details on how you can apply for maintainership in these Issue trackers like this.

[our GitLab Issue Tracker]: https://gitlab.com/ThePinsTeam-CommunityHubNetwork/fandoms/general/issues